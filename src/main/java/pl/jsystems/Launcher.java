package pl.jsystems;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pl.jsystems.config.MyConfiguration;

public class Launcher {

    public static void main(String[] args) {
        System.out.println("Launcher.main");


        ApplicationContext context =
                //new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
                new AnnotationConfigApplicationContext(MyConfiguration.class);

        System.out.println("obtaining display from context");

        Display display = context.getBean(Display.class);
        display.display();


    }
}
