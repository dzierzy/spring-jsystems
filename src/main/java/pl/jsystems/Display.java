package pl.jsystems;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class Display {

    //@Autowired
    @Resource
    private List<String> prefixList;

    private DisplaySource displaySource;

    public Display(@Qualifier("text") DisplaySource displaySource) {
        System.out.println("constructing Display object...");
        this.displaySource = displaySource;
        if(displaySource==null){
            throw new IllegalArgumentException("display source must be not null!");
        }
    }

    public void display(){
        prefixList.forEach( p -> System.out.print(p+", ") );
        System.out.println(" [ " + displaySource.getDisplayText() + " ]");
    }

    public List<String> getPrefixList() {
        return prefixList;
    }

    public void setPrefixList(List<String> prefixList) {
        this.prefixList = prefixList;
    }


}
