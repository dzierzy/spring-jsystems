package pl.jsystems;

public interface DisplaySource {

    String getDisplayText();

}
