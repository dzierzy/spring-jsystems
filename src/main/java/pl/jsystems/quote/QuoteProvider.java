package pl.jsystems.quote;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.jsystems.DisplaySource;

import javax.annotation.PostConstruct;
import java.util.logging.Logger;

@Component
@Qualifier("text")
public class QuoteProvider implements DisplaySource {

    Logger logger = Logger.getLogger(QuoteProvider.class.getName());


    @Autowired
    @Coaching(type = "serious")
    private Quote quote;

    @Override
    public String getDisplayText() {
        logger.info(quote.getQuoteText());
        return quote.getQuoteText();
    }

   /* @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("QuoteProvider: after properties set.");
    }*/

/*    @PostConstruct
    public void postConstruct(){
        logger.info("QuoteProvider: post construct");
    }*/

}
