package pl.jsystems.quote;

import org.springframework.stereotype.Component;

@Component
@Coaching(type="silly")
public class CoelhoQuote implements Quote {

    @Override
    public String getQuoteText() {
        return "Life was always a matter of waiting for the right moment to act";
    }
}
