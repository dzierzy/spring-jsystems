package pl.jsystems.quote;

public interface Quote {

    String getQuoteText();
}
