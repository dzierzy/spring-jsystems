package pl.jsystems.quote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import pl.jsystems.config.Workday;

@Component
@Coaching(type="serious")
@PropertySource({"/quote.properties","/quote2.properties"})
public class EinsteinQuote implements Quote {

    @Autowired
    private Environment env;

    @Value("#{'${quote.text}'.toUpperCase()}")
    private String quoteText;

    @Override
    @Workday
    public String getQuoteText() {
        //return env.getProperty("quote.text");
        return quoteText;
    }
}
