package pl.jsystems.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Aspect
@Component
public class MyAspect {

    //@Before("within(pl.jsystems..*)")
    @AfterReturning(pointcut="within(pl.jsystems..*)", returning = "returning")
    public void log(JoinPoint jp, String returning){
        System.out.println("exited " + jp.getSignature().toLongString() + ", returning " + returning);
    }


    @Around("within(pl.jsystems..*)")
    public Object filter(ProceedingJoinPoint jp) throws Throwable {

        Object o = jp.proceed(jp.getArgs());

        if(o instanceof String){
            String temp = (String) o;
            if(temp.contains("MC2")){
                o = temp.replace("MC2", "***");
            }
        }

        return o;
    }

    @Around("@annotation(w)")
    public Object checkIfAllowed(ProceedingJoinPoint jp, Workday w) throws Throwable {

        if(LocalDate.now().getDayOfWeek().getValue()<=5){
            System.out.println("allowing protected method execution: " + jp.toShortString());
            return jp.proceed(jp.getArgs());
        } else {
            throw new IllegalStateException("it's allowed to be invoked in workday only!");
        }
    }



}
