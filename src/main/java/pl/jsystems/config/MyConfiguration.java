package pl.jsystems.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.Arrays;
import java.util.List;

@Configuration
@ComponentScan("pl.jsystems")
@EnableAspectJAutoProxy
public class MyConfiguration {

    @Bean
    public List<String> getStringList(){
        System.out.println("executing getStringList()...");
        return Arrays.asList("from bean1", "from bean2");
    }

}
