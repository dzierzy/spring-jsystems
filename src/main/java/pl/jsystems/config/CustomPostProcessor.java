package pl.jsystems.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.logging.Logger;

@Component
public class CustomPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("class name: " + bean.getClass().getName() + ", bean name:" + beanName );

        try {
            Field f = bean.getClass().getDeclaredField("logger");
            if(f.getType().isAssignableFrom(Logger.class)){
                f.setAccessible(true);
                f.set(bean, Logger.getLogger(bean.getClass().getName()));
                f.setAccessible(false);
            }
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        return bean;
    }
}
